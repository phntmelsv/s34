// Initialization
const express = require('express')

const app = express() // initializing express as the 'app' variable

const port = 3000


// Registration of middleware
app.use(express.json()) // express.json() middleware allows our application to read JSON data

app.use(express.urlencoded({extended:true})) // urlencoded middleware allows our application to read data from forms

// 1. GET route /home: print message

app.get('/home', (request,response) => {
	response.send('Welcome to the home page')
})


// 2. Postman: GET (see collection)

// Register users
let users = [];

app.post('/register', (request,response) => {

	if(request.body.username !== '' && request.body.password !== '') {
		users.push(request.body)
		response.send(`User ${request.body.username} successfully registered!`)
	} else {
		response.send("Please input BOTH username and password.")
	}
	
})

// 3. GET route /items: retrieve all users

app.get('/users', (request,response) => {
	response.send(users)
})


// Postman: GET (see collection)

// DELETE route /delete-item: remove a user

app.delete('/delete-item', (request,response) => {
	let message;

	for(let i=0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users = users.filter(({ username }) => username !== request.body.username);

			message = `User ${request.body.username} has been deleted`

			break
		} else {
			message = "User does not exist."
		}
	}

	response.send(message)

})

// Postman: DELETE (see collection)


// Listening
app.listen(port, () => console.log(`Server is running at localhost:${port}`))